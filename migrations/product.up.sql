CREATE TABLE products (
    id    uuid primary key,
    name          varchar,
    image         varchar,
    price         int,
    discount_type varchar,
    discount      int,
    barcode       varchar
);