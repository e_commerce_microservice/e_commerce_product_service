package storage

import (
	"context"

	"gitlab.com/e_commerce_microservice/e_commerce_product_service/genproto/product_service"
)

type StorageI interface {
	CloseDB()
	Product() ProductRepoI
}

type ProductRepoI interface {
	Create(ctx context.Context, req *product_service.CreateProduct) (*product_service.CreateProductResp, error)
	// GetList(ctx context.Context, req *branch_service.BranchGetListReq) (*branch_service.BranchGetListResp, error)
	// GetById(ctx context.Context, req *branch_service.BranchIdReq) (*branch_service.Branch, error)
	// Update(ctx context.Context, req *branch_service.BranchUpdateReq) (*branch_service.BranchUpdateResp, error)
	// Delete(ctx context.Context, req *branch_service.BranchIdReq) (*branch_service.BranchDeleteResp, error)
}

type BranchProductRepoI interface {
}
