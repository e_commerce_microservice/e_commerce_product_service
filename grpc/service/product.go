package service

import (
	"context"

	"gitlab.com/e_commerce_microservice/e_commerce_product_service/config"
	"gitlab.com/e_commerce_microservice/e_commerce_product_service/genproto/product_service"
	"gitlab.com/e_commerce_microservice/e_commerce_product_service/grpc/client"
	"gitlab.com/e_commerce_microservice/e_commerce_product_service/pkg/logger"
	"gitlab.com/e_commerce_microservice/e_commerce_product_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ProductService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*product_service.UnimplementedProductServiceServer
}

func NewProductService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *ProductService {
	return &ProductService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *ProductService) Create(ctx context.Context, req *product_service.CreateProduct) (*product_service.CreateProductResp, error) {
	u.log.Info("====== Product Create ======", logger.Any("req", req))

	resp, err := u.strg.Product().Create(ctx, req)
	if err != nil {
		u.log.Error("error while creating product", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

// func (u *BranchService) GetList(ctx context.Context, req *branch_service.BranchGetListReq) (*branch_service.BranchGetListResp, error) {
// 	u.log.Info("====== Branch Get List ======", logger.Any("req", req))

// 	resp, err := u.strg.Branch().GetList(ctx, req)
// 	if err != nil {
// 		u.log.Error("error while getting branch list", logger.Error(err))
// 		return nil, status.Error(codes.InvalidArgument, err.Error())
// 	}

// 	return resp, nil
// }

// func (u *BranchService) GetById(ctx context.Context, req *branch_service.BranchIdReq) (*branch_service.Branch, error) {
// 	u.log.Info("====== Branch Get by Id ======", logger.Any("req", req))

// 	resp, err := u.strg.Branch().GetById(ctx, req)
// 	if err != nil {
// 		u.log.Error("error while getting branch by id", logger.Error(err))
// 		return nil, status.Error(codes.InvalidArgument, err.Error())
// 	}

// 	return resp, nil
// }

// func (u *BranchService) Update(ctx context.Context, req *branch_service.BranchUpdateReq) (*branch_service.BranchUpdateResp, error) {
// 	u.log.Info("====== Branch Update ======", logger.Any("req", req))

// 	resp, err := u.strg.Branch().Update(ctx, req)
// 	if err != nil {
// 		u.log.Error("error while updating branch", logger.Error(err))
// 		return nil, status.Error(codes.InvalidArgument, err.Error())
// 	}

// 	return resp, nil
// }

// func (u *BranchService) Delete(ctx context.Context, req *branch_service.BranchIdReq) (*branch_service.BranchDeleteResp, error) {
// 	u.log.Info("====== Branch Delete ======", logger.Any("req", req))

// 	resp, err := u.strg.Branch().Delete(ctx, req)
// 	if err != nil {
// 		u.log.Error("error while deleting branch", logger.Error(err))
// 		return nil, status.Error(codes.InvalidArgument, err.Error())
// 	}

// 	return resp, nil
// }
